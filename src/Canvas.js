const canvas = document.createElement('canvas');
const ctx = canvas.getContext('2d');

Object.assign(canvas, {
  init() {
    canvas.width = 640;
    canvas.height = 480;
    canvas.style.backgroundColor = '#000';
    document.body.appendChild(canvas);
  },
  ctxSession(ctx, cb) {
    ctx.save();
    cb();
    ctx.restore();
  },
});

module.exports = { canvas, ctx };
