const camera = {
  width: 450,
  height: 450,
  getPosition(focusOn) {
    const x = focusOn.x - this.width / 2;
    const y = focusOn.y - this.height / 2;
    return { x, y };
  }
};

module.exports = camera;
