class Scene {
  constructor() {
    this.items = [];
  }

  add(item) {
    this.items.push(item);
  }

  draw() {
    this.items.forEach(i => i.draw())
  }
}

module.exports = Scene;
