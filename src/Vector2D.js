class Vector2D {
  constructor(start, end) {
    this.x = start.x - end.x;
    this.y = start.y - end.y;
  }

  get len() {
    return Math.pow(this.x, 2) + Math.pow(this.y, 2);
  }

  get normalized() {
    return {
      x: this.x / this.len,
      y: this.y / this.len,
    }
  }

  get radiants() {
    return Math.atan2(this.x, this.y);
  }
}

module.exports = Vector2D;
