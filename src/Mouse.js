const { canvas } = require('./Canvas');

const mouse = {
  init() {
    this.setMousePosition = this.setMousePosition.bind(this);
    canvas.addEventListener('mousemove', this.setMousePosition);
  },

  setMousePosition(event) {
    const rect   = canvas.getBoundingClientRect(); // abs. size of element
    const scaleX = canvas.width / rect.width;      // relationship bitmap vs. element for X
    const scaleY = canvas.height / rect.height;    // relationship bitmap vs. element for Y
    this.x = (event.clientX - rect.left) * scaleX;
    this.y = (event.clientY - rect.top) * scaleY;
  },

  getMousePosition() {
    if (!canvas) throw Error('Mouse is not initiated yet!');
    return {
      x: this.x,
      y: this.y,
    };
  }
}

module.exports = mouse;
