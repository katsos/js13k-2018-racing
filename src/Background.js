const { ctx } = require('./Canvas');
const camera = require('./camera');
const IMAGE = new Image();
IMAGE.src = '../assets/map.jpg';

class Background {
  constructor(car) {
    this.car = car;
  }

  draw() {
    ctx.fillStyle = '#000';
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    const { x: cameraX, y: cameraY } = camera.getPosition({ x: this.car.xOnMap, y: this.car.yOnMap });
    ctx.drawImage(IMAGE,
      cameraX, cameraY,
      camera.width, camera.height,
      /* full viewport */
      0, 0, ctx.canvas.width, ctx.canvas.height
    );
  }
}

module.exports = Background;
