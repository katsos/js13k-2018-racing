const loop = require('./loop');
const key = require('./key');
const Car = require('./Car');
const Scene = require('./Scene');
const mouse = require('./Mouse');
const Background = require('./Background');
const { canvas } = require('./Canvas');

canvas.init();
mouse.init();

const car = new Car();
const bg = new Background(car);
const scene = new Scene();
scene.add(bg);
scene.add(car);

loop.start((dt) => {
  scene.draw();

  if (key.isDown(key.UP)) {
    car.accelerate(dt);
  }
  // if (key.isDown(key.DOWN)) {
  //   car.brake(dt);
  // }
  // if (!key.isDown(key.UP) && !key.isDown(key.DOWN)) {
  //   car.decelerate(dt);
  // }

  if (key.isDown(key.LEFT)) {
    car.steer(dt, 'left');
  }
  if (key.isDown(key.RIGHT)) {
    car.steer(dt, 'right');
  }

  car.calculateNewPosition(dt);
});
