const { canvas, ctx } = require('./Canvas');

const SCALE = 0.5;
const WIDTH = SCALE * 100;
const HEIGHT = WIDTH * 2.2;
const IMAGE = new Image();
IMAGE.src = '../assets/car_100.png';
const MAX_VELOCITY = 240;
const ACCELERATION = 3;
const STEERING_FACTOR = 50;

class Car {
  constructor() {
    this.width = WIDTH;
    this.height = HEIGHT;
    this.image = IMAGE;
    this.xOnScreen = canvas.width/2;
    this.yOnScreen = canvas.height/2;
    this.xOnMap = 100;
    this.yOnMap = 100;
    this.velocity = 0;
    this.direction = 0;
  }

  draw() {
    canvas.ctxSession(ctx, () => {
      // set pivot point
      ctx.translate(this.xOnScreen, this.yOnScreen);
      // rotate around pivot point
      ctx.rotate(this.direction * Math.PI/180);
      ctx.drawImage(
        this.image,
        -(this.width/2),
        -(this.height/2),
        this.width,
        this.height
      );
    })
  }

  steer(dt, direction) {
    const speed = dt * STEERING_FACTOR;
    if (direction === 'left') this.direction -= speed;
    else this.direction += speed;
  }

  accelerate(dt) {
    if (this.velocity >= MAX_VELOCITY) return;
    this.velocity += ACCELERATION * dt;
    if (this.velocity > MAX_VELOCITY) this.velocity = MAX_VELOCITY;
  }

  calculateNewPosition(dt) {
    const speed = this.velocity * dt;
    this.xOnMap += Math.cos(this.direction) * speed;
    this.yOnMap += Math.sin(this.direction) * speed;
  }
}

module.exports = Car;

